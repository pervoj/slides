import Reveal from "reveal.js";
import RevealNotes from "reveal.js/plugin/notes/notes";
import RevealHighlight from "reveal.js/plugin/highlight/highlight";

const variantAttrKey = "data-variant";
const controlsColorAttrKey = "data-controls-color";
const controlsColorPropKey = "--slide-controls";

// Init reveal.
await Reveal.initialize({
  width: 1920,
  height: 1080,
  hash: true,
  respondToHashChanges: true,
  fragmentInURL: true,
  plugins: [RevealNotes, RevealHighlight],
  // backgroundTransition: "zoom",
});

// Select all elements we are working with.
const controls = document.querySelector(".reveal .controls") as HTMLElement;
const progress = document.querySelector(".reveal .progress") as HTMLElement;
const slides = document.querySelectorAll(".reveal .slides section");
const bgs = document.querySelectorAll(".reveal .backgrounds .slide-background");

// Setup variants.
for (const i of slides.keys()) {
  const slide = slides[i];
  const background = bgs[i];

  const variant = slide.getAttribute(variantAttrKey);
  if (!variant) continue;
  background.setAttribute(variantAttrKey, variant);
}

// Update controls color.
function updateControlsColor() {
  const slide = Reveal.getCurrentSlide();
  const styles = window.getComputedStyle(slide);

  const accent = slide.hasAttribute(controlsColorAttrKey)
    ? slide.getAttribute(controlsColorAttrKey)
    : styles.getPropertyValue(controlsColorPropKey);

  controls.style.setProperty("color", accent);
  progress.style.setProperty("color", accent);
}
Reveal.on("slidechanged", updateControlsColor);
updateControlsColor();
